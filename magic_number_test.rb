require_relative 'magic_number'
require 'test/unit'
require 'prime'

class MagicNumberTest < Test::Unit::TestCase
  def test_sum_magic_numbers
    assert_equal(3, sum_magic_numbers([[8, 27], [49, 49]]))
  end

  def test_sum_magic_numbers_with_three_intervals
    assert_equal(8, sum_magic_numbers([[8, 49], [8, 52], [8, 40]]))
  end

  def test_sum_magic_numbers_for_magic_number_present_in_one_interval
    assert_equal(2, sum_magic_numbers([[8, 27], [50, 52]]))
  end

  def test_sum_magic_numbers_for_inervals_with_no_magic_number
    assert_equal(0, sum_magic_numbers([[28, 40], [50, 52]]))
  end

  def test_generate_numbers
    assert_equal([1, 2, 3, 5, 7, 9], generate_numbers([1, 10]))
  end

  def test_generate_numbers_for_interval_of_one
    assert_equal([1], generate_numbers([1, 1]))
  end

  def test_generate_numbers_for_interval_of_even
    assert_equal([], generate_numbers([4, 4]))
  end

  def test_is_magic_number_for_magic_number
    assert_equal(true, is_magic_number?(9))
  end

  def test_is_magic_number_for_not_magic_number
    assert_equal(false, is_magic_number?(20))
  end
  
  def test_is_prime_for_prime_numbers
    Prime.each(100) do |prime|
      assert_equal(true, is_prime?(prime))
    end
  end

  def test_is_prime_for_not_prime_numbers
    (4..100).step(2).each do |n|
      assert_equal(false, is_prime?(n))
    end
  end

  def test_is_prime_for_two
    assert_equal(true, is_prime?(2))
  end
end
