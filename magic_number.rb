def sum_magic_numbers(intervals)
  numbers_per_interval = intervals.map { |interval| generate_numbers(interval) }

  magic_numbers = numbers_per_interval.map do |number|
    number.select { |n| is_magic_number?(n) }
  end
  
  magic_numbers.map(&:count).reduce(:+)
end

def generate_numbers(interval)
  (interval[0]..interval[1]).select do |n| 
    n if n.odd? || n == 2
  end
end

def is_magic_number?(number)
  sqrt = Math.sqrt(number)

  is_prime?(sqrt)
end

def is_prime?(number)
  (number > 1) &&
  (number % 1 == 0) &&
  (2..number/2).none? { |i| number % i == 0 }
end
