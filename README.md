# Numeros Mágicos

Um número X é dito “mágico” quando a raiz quadrada de X existe e é um número primo. Escreva um programa que receba como entrada uma lista de intervalos [A,B] e retorne o somatório da quantidade de números mágicos encontrados em cada intervalo. É garantido que os números A e B serão inteiros positivos e que A será sempre menor ou igual que B
Para a entrada: [[8,27], [49,49]] Resultado: 3
Seriam os números 9 e 25 do primeiro intervalo e 49 do segundo

## A Solução
A solução foi escrita em ruby, uma linguagem que pessoalmente gosto bastante e me sinto confortável, e não possui dependências externas. Foi escrito na versão 2.3.3 porém não é um requisito para rodar o script, que foi testado e funciona nas versões > 2. Os testes foram escritos em test unit devido a simplicidade e por não exigir instalação ou setup prévio.

O script é bem simples, está no arquivo `magic_number.rb` e foi dividido em quatro métodos:
* `is_prime?(number)`: recebe um número e retorna se ele é primo
* `is_magic_number?(number)`: recebe um número e retorna se ele é um número mágico
* `generate_numbers(interval)`: dado um intervalo é gerado uma lista com os números desse intervalo (apenas os números ímpares e o número dois)
* `sum_magic_numbers(intervals)`: dado uma lista de intervalos retorna o somatório da quantidade de numeros mágicos nestes intervalos

### Uso
Não é necessário setup.

```sh
$ ruby magic_number.rb
$ ruby magic_number_test.rb
```
